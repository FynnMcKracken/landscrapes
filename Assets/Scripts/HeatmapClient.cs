﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class HeatmapClient: MonoBehaviour
{
    public static readonly byte[] CMD_NEXT = {0x0};
    public static readonly byte[] CMD_QUIT = {0x1};
    public static readonly byte[] CMD_SHUTDOWN = {0x2};

    private int RESPONSE_SIZE = Marshal.SizeOf<HeatmapModel>();

    private Image _heatmapImage;

    public String Host = "192.168.2.151";

    public ushort Port = 50001;

    public uint SkipFrames = 1;

    public ChangeDelegate OnChange;
    
    public delegate void ChangeDelegate(HeatmapModel updatedHeatmap);

    private Socket _socket;

    protected void Connect()
    {
        if (_socket == null)
        {
            Debug.Log(String.Format("Trying connecting to server {0}:{1}", this.Host, this.Port));
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.Connect(this.Host, this.Port);
            if (socket.Connected)
            {
                Debug.Log(String.Format("Connected to server {0}:{1}", this.Host, this.Port));
                _socket = socket;
            }
        }
        
    }

    protected void Disconnect()
    {
        if (_socket != null)
        {
            _socket.Send(CMD_QUIT);
            _socket.Close();
            _socket = null;
        }
    }

    protected void Reconnect()
    {
        Disconnect();
        Connect();
    }

    private void OnEnable()
    {
        try
        {
            this.Reconnect();
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }

        StartCoroutine(nameof(this.UpdateCoroutine));
    }

    private void OnDisable()
    {
        Disconnect();
    }

    private IEnumerator UpdateCoroutine()
    {
        while (enabled)
        {
            if (_socket != null && this._socket.Connected)
            {
                try
                {
                    HandleResponse();
                }
                catch (Exception e1)
                {
                    Debug.LogError(e1);
                    try
                    {
                        Reconnect();
                    }
                    catch (Exception e2)
                    {
                        Debug.LogError(e2);
                        _socket = null;
                    }

                    continue;
                }
            }
            else
            {
                try
                {
                    Reconnect();
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                }
            }

            for (int i = 0; i < Math.Max(1, this.SkipFrames); i++)
            {
                yield return new WaitForEndOfFrame();
            }
        }
    }

    private byte[] responseBuffer = new byte[16900];
    private int bytesReceived = 0;
    
    protected void HandleResponse()
    {
        /*if (responseBuffer == null)
        {
            Debug.Log("HandleResponse --- Building buffer with " + responseBuffer.Length + " bytes.");
            responseBuffer = new byte[RESPONSE_SIZE];
        }*/

        if (_socket.Available < 16900)
            return;

        bytesReceived += _socket.Receive(responseBuffer, bytesReceived, responseBuffer.Length - bytesReceived, SocketFlags.None);
        Debug.Log("HandleResponse --- Received " + bytesReceived + " bytes.");
        
        if (bytesReceived == responseBuffer.Length)
        {
            Debug.Log("HandleResponse --- Received full heatmap!");
            
            GCHandle handle = GCHandle.Alloc(responseBuffer, GCHandleType.Pinned);
            HeatmapModel newHeatmap = (HeatmapModel) Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(HeatmapModel));
            
            handle.Free();
            responseBuffer = new byte[16900];
            bytesReceived = 0;

            if (OnChange != null)
            {
                try {
                    Debug.Log("HandleResponse --- New heatmap: ");
                    OnChange.Invoke(newHeatmap);
                } catch (Exception e) {
                    Debug.LogError (e);
                }
            }
            else
            {
                Debug.Log("HandleResponse --- OnChange has no listeners.");
            }
       
        } 
        else if (bytesReceived > responseBuffer.Length)
        {
            responseBuffer = new byte[16900];
            bytesReceived = 0; 
        }

    }
}