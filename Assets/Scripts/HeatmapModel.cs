﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;

[StructLayout(LayoutKind.Sequential, Pack = 1)]
public class HeatmapModel
{
    
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 65 * 65)]
    public float[] heights = new float[65*65];
    
}
