﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public class TerrainMorpher: MonoBehaviour
{
    private Mesh mesh;

    private Vector3[] vertices = new Vector3[(xSize + 1) * (zSize + 1)];
    private int[] triangles;
    private Color[] colors;

    private static int xSize = 104;
    private static int zSize = 77;
    public int scaling = 1;
    public Gradient gradient;
    public Terrain terrain;

    private float minTerrainHeight;
    private float maxTerrainHeight;
    
    private float nextUpdate = 0f;
    
    private HeatmapModel _heatmapModel = new HeatmapModel();
    private HeatmapModel _oldHeatmapModel = new HeatmapModel();
    
    // Start is called before the first frame update
    void Start()
    {
        mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;

        CreateShape();
        UpdateMesh();
    }

    private void OnEnable()
    {
        GameObject gameController = GameObject.FindWithTag ("GameController");
        if (gameController) {
            HeatmapClient heatmapClient = gameController.GetComponent<HeatmapClient> ();
            if (heatmapClient)
            {
                Debug.Log("TerrainMorpher --- Registered to OnChange of GCO");
                heatmapClient.OnChange += this.OnHeatmapChanged;
            }
            else 
                this.enabled = false;
         
        }
    }
    
    private void OnDisable () {
        GameObject gameController = GameObject.FindWithTag ("GameController");
        if (gameController) {
            HeatmapClient heatmapClient = gameController.GetComponent<HeatmapClient> ();
            if (heatmapClient) 
                heatmapClient.OnChange -= this.OnHeatmapChanged;
        }
    }

    private void OnHeatmapChanged(HeatmapModel updatedheatmap)
    {
        Debug.Log("TerrainMorpher --- Received new Heatmap with size " + updatedheatmap.heights.Length);
        _oldHeatmapModel = _heatmapModel;
        _heatmapModel = updatedheatmap;
    }
        
    private void CreateShape()
    {                
        
        for (int i= 0, z = 0; z < zSize; z++)
        {
            for (int x = 0; x < xSize; x++)
            {
                //float y = Mathf.PerlinNoise(x * .3f, z * .3f) * 2f;
                float y = _heatmapModel.heights[(z * xSize) + x] * 5f;
                vertices[i] = new Vector3(x, y, z);
              
                i++;
            }
        }

        
        triangles = new int[xSize * zSize * 6];

        int vert = 0;
        int tris = 0;
        for (int z = 0; z < zSize; z++)
        {
            for (int x = 0; x < xSize; x++)
            {
                triangles[tris + 0] = vert + 0;
                triangles[tris + 1] = vert + xSize + 1;
                triangles[tris + 2] = vert + 1;  
                triangles[tris + 3] = vert + 1;  
                triangles[tris + 4] = vert + xSize + 1;  
                triangles[tris + 5] = vert + xSize + 2;

                vert++;
                tris += 6;
            }

            vert++;
        }
        
        /*colors = new Color[vertices.Length];

        for (int i = 0, z = 0; z < zSize; z++)
        {
            for (int x = 0; x <= xSize; x++)
            {
                float height = Mathf.InverseLerp(minTerrainHeight, maxTerrainHeight, vertices[i].y);
                colors[i] = gradient.Evaluate(height);
                i++;
            }
        }*/
        
        
    }

    private void UpdateShape()
    {
        for (int i= 0, z = 0; z < zSize; z++)
        {
            for (int x = 0; x < xSize; x++)
            {
                float y = Mathf.Lerp( 
                    vertices[(z * xSize) + x].y, 
                    _heatmapModel.heights[(z * xSize) + x] * 5f, 
                    Time.deltaTime);
                vertices[i] = new Vector3(x, y, z);
              
                i++;
            }
        }
        
    }

    void UpdateMesh()
    {
        mesh.Clear();

        mesh.vertices = vertices;
        mesh.triangles = triangles;
        //mesh.colors = colors;
        
        mesh.RecalculateNormals();

    }

    private void OnDrawGizmos()
    {
        if (vertices == null)
        {
            return;
        }

        for (int i = 0; i < vertices.Length; i++)
        {
            Gizmos.DrawSphere(vertices[i], .1f);
        }
            
    }


    // Update is called once per frame
    void Update()
    {
        if(Time.time >= nextUpdate){
            nextUpdate += 0.1f;
            UpdateShape();
            UpdateMesh();
        }
    }
}
