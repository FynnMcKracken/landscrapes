﻿using UnityEngine;

public class CameraRotate : MonoBehaviour
{

    public GameObject rotateAround;

    void Update()
    {
        transform.LookAt(rotateAround.transform.position);
        transform.RotateAround(rotateAround.transform.position, Vector3.up, Time.deltaTime * 10f);

    }
}
