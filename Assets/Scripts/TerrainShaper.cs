﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Numerics;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

[RequireComponent(typeof(MeshFilter))]
public class TerrainShaper : MonoBehaviour
{
    private Terrain _terrain;
    private TerrainData _terrainData;
    private int _xRes;
    private int _yRes;

    private float[,,] _splatmapData;

    private enum TerrainAltitude
    {
        None = 0,
        Low = 1,
        Mid = 2,
        High = 3
    }

    private enum EdgeOrientation
    {
        X0,
        XZ,
        Z0,
        ZX
    }

    class DetailInstance
    {
        public Vector2Int Position;
        public TerrainAltitude Altitude;

        public DetailInstance(Vector2Int position, TerrainAltitude altitude)
        {
            Position = position;
            Altitude = altitude;
        }
    }

    private TerrainAltitude[] _treeAltitudes;

    private List<DetailInstance> _detailPositions;

    private float[,] _heights;

    private float _nextUpdate = 0f;
    private float _nextUpdateSlow = 0f;

    private HeatmapModel _heatmapModel = new HeatmapModel();

    public float growVelocity = .2f;


    public float terrainEdgeBottom = -20f;
    public float uvFactor = 2f;

    public GameObject borderX0;
    private Mesh _meshBorderX0;

    public GameObject borderXZ;
    private Mesh _meshBorderXZ;

    public GameObject borderZ0;
    private Mesh _meshBorderZ0;

    public GameObject borderZX;
    private Mesh _meshBorderZX;

    // Start is called before the first frame update
    void Start()
    {
        _terrain = transform.GetComponent<Terrain>();
        _terrainData = _terrain.terrainData;
        _splatmapData = new float[_terrainData.alphamapWidth, _terrainData.alphamapHeight, _terrainData.alphamapLayers];
        _treeAltitudes = new TerrainAltitude[_terrainData.treeInstanceCount];
        for (int i = 0; i < _treeAltitudes.Length; i++)
            _treeAltitudes[i] = TerrainAltitude.None;

        _xRes = _terrainData.heightmapWidth;
        _yRes = _terrainData.heightmapHeight;

        _meshBorderX0 = new Mesh();
        borderX0.GetComponent<MeshFilter>().mesh = _meshBorderX0;

        _meshBorderXZ = new Mesh();
        borderXZ.GetComponent<MeshFilter>().mesh = _meshBorderXZ;

        _meshBorderZ0 = new Mesh();
        borderZ0.GetComponent<MeshFilter>().mesh = _meshBorderZ0;

        _meshBorderZX = new Mesh();
        borderZX.GetComponent<MeshFilter>().mesh = _meshBorderZX;

        InitDetailPositions();
        ResetPoints();
    }

    private void InitDetailPositions()
    {
        ResetDetails();
        _detailPositions = new List<DetailInstance>();
        float rnd = 0f;
        for (int x = 0; x < _terrainData.detailWidth; x++)
        {
            for (int y = 0; y < _terrainData.detailHeight; y++)
            {
                //rnd = Mathf.PerlinNoise(0.1f * x / _terrainData.detailHeight, 0.1f * y / _terrainData.detailWidth);
                rnd = Random.Range(0f, 1f);
                //Debug.Log("Noise: " + rnd);
                if (rnd > 0.9f)
                {
                    //Debug.Log("Adding new detail at " + x + ", " + y);
                    _detailPositions.Add(new DetailInstance(new Vector2Int(x, y), TerrainAltitude.None));
                }
            }
        }

        Debug.Log("Details: " + _detailPositions.Count);
    }

    void ResetPoints()
    {
        float[,] heights = _terrainData.GetHeights(0, 0, _xRes, _yRes);
        for (int y = 0; y < _yRes; y++)
        {
            for (int x = 0; x < _xRes; x++)
            {
                heights[x, y] = .5f;
            }
        }

        _terrainData.SetHeights(0, 0, heights);
    }

    private void OnEnable()
    {
        GameObject gameController = GameObject.FindWithTag("GameController");
        if (gameController)
        {
            HeatmapClient heatmapClient = gameController.GetComponent<HeatmapClient>();
            if (heatmapClient)
            {
                Debug.Log(this.name + " --- Registered to OnChange of GCO");
                heatmapClient.OnChange += this.OnHeatmapChanged;
            }
            else
                this.enabled = false;
        }
    }

    private void OnDisable()
    {
        GameObject gameController = GameObject.FindWithTag("GameController");
        if (gameController)
        {
            HeatmapClient heatmapClient = gameController.GetComponent<HeatmapClient>();
            if (heatmapClient)
                heatmapClient.OnChange -= this.OnHeatmapChanged;
        }
    }

    private void OnHeatmapChanged(HeatmapModel updatedheatmap)
    {
        Debug.Log(this.name + " --- Received new Heatmap with size " + updatedheatmap.heights.Length);
        _heatmapModel = updatedheatmap;
    }

    private void UpdateTerrain()
    {
        _heights = _terrainData.GetHeights(0, 0, _xRes, _yRes);

        for (int y = 0; y < _yRes; y++)
        {
            for (int x = 0; x < _xRes; x++)
            {
                _heights[x, y] = Mathf.Lerp(_heights[x, y], (1f - _heatmapModel.heights[(y * _xRes) + x]),
                    Time.deltaTime * growVelocity);
            }
        }

        _terrainData.SetHeights(0, 0, _heights);
    }

    private void UpdateTrees()
    {
        TreeInstance[] treeInstances = _terrainData.treeInstances;
        for (int i = 0; i < treeInstances.Length; i++)
        {
            float posY = treeInstances[i].position.y;

            if (posY < 0.33 && _treeAltitudes[i] != TerrainAltitude.Low)
            {
                treeInstances[i].prototypeIndex = Random.Range(6, 11);
                _treeAltitudes[i] = TerrainAltitude.Low;
            }
            else if (posY >= 0.33 && posY < 0.66 && _treeAltitudes[i] != TerrainAltitude.Mid)
            {
                treeInstances[i].prototypeIndex = Random.Range(0, 5);
                _treeAltitudes[i] = TerrainAltitude.Mid;
            }
            else if (posY >= 0.66 && _treeAltitudes[i] != TerrainAltitude.High)
            {
                treeInstances[i].prototypeIndex = Random.Range(12, 16);
                _treeAltitudes[i] = TerrainAltitude.High;
            }
        }

        _terrainData.SetTreeInstances(treeInstances, true);
    }

    private void UpdateDetails()
    {
        var map0 = _terrainData.GetDetailLayer(0, 0, _terrainData.detailWidth, _terrainData.detailHeight, 19);
        var map1 = _terrainData.GetDetailLayer(0, 0, _terrainData.detailWidth, _terrainData.detailHeight, 12);
        var map2 = _terrainData.GetDetailLayer(0, 0, _terrainData.detailWidth, _terrainData.detailHeight, 1);

        for (int i = 0; i < _detailPositions.Count; i++)
        {
            TerrainAltitude altitude = _detailPositions[i].Altitude;
            int x = _detailPositions[i].Position.x;
            int y = _detailPositions[i].Position.y;
            float normX = x * 1.0f / (_terrainData.detailWidth - 1);
            float normY = y * 1.0f / (_terrainData.detailHeight - 1);
            var height = _terrainData.GetInterpolatedHeight(normX, normY) / 20f; 
            
            if (height < 0.33 && _detailPositions[i].Altitude != TerrainAltitude.Low)
            {
                map0[x, y] = 1;
                map1[x, y] = 0;
                map2[x, y] = 0;
                _detailPositions[i].Altitude = TerrainAltitude.Low;
            }
            else if (height >= 0.33 && height < 0.66 && _detailPositions[i].Altitude != TerrainAltitude.Mid)
            {
                map0[x, y] = 0;
                map1[x, y] = 1;
                map2[x, y] = 0;
                _detailPositions[i].Altitude = TerrainAltitude.Mid;
            }
            else if (height >= 0.66 && _detailPositions[i].Altitude != TerrainAltitude.High)
            {
                map0[x, y] = 0;
                map1[x, y] = 0;
                map2[x, y] = 1;
                _detailPositions[i].Altitude = TerrainAltitude.High;
            }
            
            
            map0[x, y] = 1;
        }

        // Assign the modified map back.
        _terrainData.SetDetailLayer(0, 0, 19, map0);
        _terrainData.SetDetailLayer(0, 0, 12, map1);
        _terrainData.SetDetailLayer(0, 0, 1, map2);
    }

    private void ResetDetails()
    {
        var map = _terrainData.GetDetailLayer(0, 0, _terrainData.detailWidth, _terrainData.detailHeight, 0);

        for (var y = 0; y < _terrainData.detailHeight; y++)
        {
            for (var x = 0; x < _terrainData.detailWidth; x++)
            {
                    map[x, y] = 0;
            }
        }

        _terrainData.SetDetailLayer(0, 0, 0, map);
        _terrainData.SetDetailLayer(0, 0, 12, map);
        _terrainData.SetDetailLayer(0, 0, 1, map);
        
    }

    private void UpdateTerrainSplatmap()
    {
        for (int y = 0; y < _terrainData.alphamapHeight; y++)
        {
            for (int x = 0; x < _terrainData.alphamapWidth; x++)
            {
                float normX = x * 1.0f / (_terrainData.alphamapWidth - 1);
                float normY = y * 1.0f / (_terrainData.alphamapHeight - 1);

                var angle = _terrainData.GetSteepness(normX, normY);
                var fracSteepness = angle / 90.0;

                var height = _terrainData.GetInterpolatedHeight(normX, normY);
                var fracHeight = height / 20;

                var lowFlatTextureAlpha = 0f;
                var lowSteepTextureAlpha = 0f;
                var highFlatTextureAlpha = 0f;
                var highSteepTextureAlpha = 0f;

                // Low & flat
                if (fracHeight <= 0.4)
                {
                    lowFlatTextureAlpha = 1f;
                }
                else if (fracHeight > 0.4 && fracHeight <= 0.6)
                {
                    lowFlatTextureAlpha = 1 - fracHeight;
                    highFlatTextureAlpha = fracHeight;
                }
                else
                {
                    highFlatTextureAlpha = 1f;
                }

                // Low & flat
                _splatmapData[y, x, 0] = (float) (1 - fracSteepness) * lowFlatTextureAlpha;

                // Low & steep
                _splatmapData[y, x, 1] = (float) fracSteepness * lowFlatTextureAlpha;

                // High & flat
                _splatmapData[y, x, 2] = (float) (1 - fracSteepness) * highFlatTextureAlpha;

                // High & steep
                _splatmapData[y, x, 3] = (float) fracSteepness * highFlatTextureAlpha;
            }
        }

        _terrainData.SetAlphamaps(0, 0, _splatmapData);
    }

    private void UpdateBorders()
    {
        UpdateEdge(_meshBorderX0, EdgeOrientation.X0);
        UpdateEdge(_meshBorderXZ, EdgeOrientation.XZ);
        UpdateEdge(_meshBorderZ0, EdgeOrientation.Z0);
        UpdateEdge(_meshBorderZX, EdgeOrientation.ZX);
    }

    // Only works for square terrain
    private void UpdateEdge(Mesh mesh, EdgeOrientation edgeOrientation)
    {
        int numVertices = ((int) _terrainData.size.x + 1) * 2;

        Vector3[] vertices = new Vector3[numVertices];
        Vector2[] uvs = new Vector2[numVertices];

        int numTriangles = ((int) _terrainData.size.x) * 6;
        int[] triangles = new int[numTriangles];

        Func<float, float> getHeightForOrientation = null;
        Func<float, float, Vector3> getVertexTopForOrientation = null;
        Func<float, float, Vector3> getVertexBottomForOrientation = null;

        bool invertNorms = false;

        switch (edgeOrientation)
        {
            case EdgeOrientation.X0:
                getHeightForOrientation = pos => _terrainData.GetInterpolatedHeight(pos, 0.001f);
                getVertexTopForOrientation =
                    (index, height) => new Vector3(index, height, 0) + _terrain.transform.position;
                getVertexBottomForOrientation = (index, height) =>
                    new Vector3(index, terrainEdgeBottom, 0) + _terrain.transform.position;
                break;
            case EdgeOrientation.XZ:
                getHeightForOrientation = pos => _terrainData.GetInterpolatedHeight(pos, 0.999f);
                getVertexTopForOrientation = (index, height) =>
                    new Vector3(index, height, _terrainData.size.z) + _terrain.transform.position;
                getVertexBottomForOrientation = (index, height) =>
                    new Vector3(index, terrainEdgeBottom, _terrainData.size.z) + _terrain.transform.position;
                invertNorms = true;
                break;
            case EdgeOrientation.Z0:
                getHeightForOrientation = pos => _terrainData.GetInterpolatedHeight(0.001f, pos);
                getVertexTopForOrientation =
                    (index, height) => new Vector3(0, height, index) + _terrain.transform.position;
                getVertexBottomForOrientation = (index, height) =>
                    new Vector3(0, terrainEdgeBottom, index) + _terrain.transform.position;
                invertNorms = true;
                break;
            case EdgeOrientation.ZX:
                getHeightForOrientation = pos => _terrainData.GetInterpolatedHeight(0.999f, pos);
                getVertexTopForOrientation = (index, height) =>
                    new Vector3(_terrainData.size.x, height, index) + _terrain.transform.position;
                getVertexBottomForOrientation = (index, height) =>
                    new Vector3(_terrainData.size.x, terrainEdgeBottom, index) + _terrain.transform.position;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(edgeOrientation), edgeOrientation, null);
        }


        for (float i = 0; i <= _terrainData.size.x; i++)
        {
            float pos = i / _terrainData.size.x;
            float height = getHeightForOrientation(pos);

            float normHeight = height / (_terrainData.size.y);

            vertices[(int) i * 2] = getVertexTopForOrientation(i, height);
            vertices[(int) i * 2 + 1] = getVertexBottomForOrientation(i, height);

            float bottomUvOffset = terrainEdgeBottom / _terrainData.size.y;
            uvs[(int) i * 2] = new Vector2(pos * uvFactor, normHeight * uvFactor);
            uvs[(int) i * 2 + 1] = new Vector2(pos * uvFactor, bottomUvOffset * uvFactor);
        }

        for (int i = 0; i < numTriangles / 3; i++)
        {
            triangles[i * 3] = i + (1 - (i % 2));
            triangles[i * 3 + 1] = i + (invertNorms ? 2 : i % 2);
            triangles[i * 3 + 2] = i + (invertNorms ? i % 2 : 2);
        }

        mesh.Clear();
        mesh.vertices = vertices;
        mesh.uv = uvs;
        mesh.triangles = triangles;
        mesh.RecalculateNormals();
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time >= _nextUpdate)
        {
            _nextUpdate += 0.1f;
            UpdateTerrain();
            UpdateTerrainSplatmap();
            UpdateTrees();
            UpdateBorders();
            //UpdateDetails();
        }
    }
}