﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomHeights : MonoBehaviour
{
    private Terrain terrain;
    private TerrainData terrainData;
    private int xRes;
    private int yRes;
    
    float[,] heights;
    
    private float nextUpdate;
    
    void Start()
    {
        terrain = transform.GetComponent<Terrain>();
        
        terrainData = terrain.terrainData;
         
        xRes = terrainData.heightmapWidth;
        yRes = terrainData.heightmapHeight;
        
    }
    
    void randomizePoints(float strength) { 
        heights = terrainData.GetHeights(0, 0, xRes, yRes);
         
        for (int y = 0; y < yRes; y++) {
            for (int x = 0; x < xRes; x++)
            {
                if (y % 77 == 0)
                    heights[x, y] = Random.Range(0.0f, strength) * 0.5f;
                else
                    heights[x, y] = 0f;
            }
        }
         
        terrainData.SetHeightsDelayLOD(0, 0, heights);
    }
    
    void resetPoints() { var heights = terrainData.GetHeights(0, 0, xRes, yRes);
        for (int y = 0; y < yRes; y++) {
            for (int x = 0; x < xRes; x++) {
                heights[x,y] = 0;
            }
        }
         
        terrainData.SetHeights(0, 0, heights);
    } 

    
    void OnGUI() {
        if(GUI.Button (new Rect (10, 10, 100, 25), "Wrinkle")) {
            randomizePoints(0.1f);
        }
         
        if(GUI.Button (new Rect (10, 40, 100, 25), "Reset")) {
            resetPoints();
        } 
    }
    
    void Update()
    {
        if(Time.time >= nextUpdate){
            nextUpdate += 0.1f;
            UpdateSlow();
        }
        
    }

    private void UpdateSlow()
    {
        randomizePoints(0.1f);
    }
}
